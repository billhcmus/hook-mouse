﻿#pragma once
// Hàm khởi tạo hook
HWND hWndServer;
HHOOK hHook;
extern HINSTANCE hInstance;

// Hàm hồi quy của chuột
LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);

// Hàm khởi tạo mouse hook
BOOL setMouseHook(HWND hWnd);

// Hàm hủy mouse hook
BOOL clearMouseHook(HWND hWnd);