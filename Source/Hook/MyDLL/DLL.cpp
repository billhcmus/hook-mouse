#include "stdafx.h"
#include "DLL.h"

BOOL setMouseHook(HWND hWnd)
{
	if (hWndServer != NULL)
	{
		return FALSE;
	}

	hHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)MouseProc, hInstance, 0);
	if (hHook != NULL)
	{
		hWndServer = hWnd;
		return TRUE;
	}
	return FALSE;
}

BOOL clearMouseHook(HWND hWnd)
{
	if (hWnd != hWndServer)
	{
		return FALSE;
	}
	if (UnhookWindowsHookEx(hHook))
	{
		hWndServer = NULL;
		return TRUE;
	}
	return FALSE;
}

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		switch (wParam)
		{
		case WM_LBUTTONDOWN:
			return TRUE;
		case WM_LBUTTONUP:
			return TRUE;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}
