﻿# Thông tin cá nhân
- MSSV: 1512557
- Họ tên: Phan Trọng Thuyên
# Bài tập ứng dụng Hook
# Các chức năng đã làm được
1. Khi kích hoạt với phím tắt bất kì do bạn qui định, tự động vô hiệu hóa chuột trái (bấm chuột trái như không bấm). Chỉ trở về bình thường khi bấm phím tắt đã định ban đầu.

## Yêu cầu nâng cao
1. Đưa các hàm xử lý hook vào dll

## Các luồng sự kiện chính
> Chạy chương trình
> Nhấn S để vô hiệu hoá chuột trái
> Nhấn S lần nữa để kích hoạt chuột trái

## Yêu cầu khác
> Link repo: https://billhcmus@bitbucket.org/billhcmus/hook-mouse.git

> Link video: https://youtu.be/6DhkoB0_Pxg

> Nền tảng build: Visual Studio 2017